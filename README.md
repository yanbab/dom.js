# dom.js

Web Components made simple

## Quick start

Include dom.js in your html page : (2k compressed)
    
    <script src="https//cdn.com/dom.js"/>

It provides you a new &lt;template /> tag, used to define our reusable component.

### Example : Hello World

    <HelloWorld/>
    
    <template for="HelloWorld" >   
        Hello world !
    </template>
  
### Example: Custom button

Here we pass a custom 'label' property to our component, accessed with ${label}

    <MyButton label="Hello !" />
    
    <template for="MyButton" >   
        <button>${label}</button>
        <style>
        button { background: yellow; }
        </style>
    </template>
  
### Example: External template

Organize and reuse your components with files

    <MyButton label="Hello !" />
    <template src="MyButton.html" />

*MyButton.html*

    <!- MyButton -->
    <button>${label}</button>
    <script></script>
    <style></style>
